# opendurian-demo

 - MPEG-DASH with Encrypted Media Extensions (EME)
 - Simple CDN to cache video from CloudFront/S3

## MPEG-DASH with Encrypted Media Extensions (EME)
Convert any video to MPEG-Dash with DRM

Install prerequisites
>sudo apt install ffmpeg

>sudo apt install gpac

Run command
>./convert2dash.sh

## Simple CDN to cache video from CloudFront/S3

![SimpleCDN](https://bitbucket.org/Eittipat/opendurian-demo/raw/master/simple-cdn/SimpleCDN.png)

See Nginx configuration file
>nginx-cdn.conf

## References

