#!/bin/bash

# Get video/audio track
ffmpeg -i source/sample.mp4 -c:a copy -vn temp/sample-audio.mp4
ffmpeg -i source/sample.mp4 -c:v libx264 -an -vf scale=-1:242 temp/sample-240.mp4
ffmpeg -i source/sample.mp4 -c:v libx264 -an -vf scale=-1:720 temp/sample-720.mp4

# Encrypt video
MP4Box -crypt drm.xml temp/sample-audio.mp4 -out temp/sample-audio-encrypted.mp4
MP4Box -crypt drm.xml temp/sample-240.mp4 -out temp/sample-240-encrypted.mp4
MP4Box -crypt drm.xml temp/sample-720.mp4 -out temp/sample-720-encrypted.mp4

# Convert To Dash
MP4Box -dash 10000 -bs-switching no -segment-name %s -out dash/sample_dash.mpd \
    temp/sample-720-encrypted.mp4#video \
    temp/sample-240-encrypted.mp4#video \
    temp/sample-audio-encrypted.mp4#audio


